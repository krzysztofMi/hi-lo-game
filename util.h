#ifndef UTIL_H
#define UTIL_H
#include <limits>
#include <random>
#include <chrono>

template <typename T>
std::uniform_int_distribution<T> getDistribution(std::true_type, T min, T max){
    return std::uniform_int_distribution<T>(min, max);
}
template <typename T>
std::uniform_real_distribution<T> getDistribution(std::false_type, T min, T max){
    return std::uniform_real_distribution<T>(min, max);
}

template <typename T>
T generateQuest(){
    auto seed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
    std::mt19937 gen(seed);
    auto dist = getDistribution<T>(std::integral_constant<bool,
                                                          std::numeric_limits<T>::is_integer>(),
                                   std::numeric_limits<T>::min(),
                                   std::numeric_limits<T>::max());
    return dist(gen);
}

template <typename T>
T generateQuest(T min, T max){
    auto seed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
    std::mt19937 gen(seed);
    auto dist = getDistribution<T>(std::integral_constant<bool,
                                                          std::numeric_limits<T>::is_integer>(),
                                   min, max);
    return dist(gen);
}

#endif // UTIL_H
