#ifndef PLAYER_H
#define PLAYER_H

#include <limits>
#include <iostream>
#include "util.h"
class Player
{
    static unsigned playerCount;
    int id;
    int feedbackFlag;
public:
    Player();
    unsigned GetID();
    template<typename T>
    void feedback(int number){
        feedbackFlag = number;
    }
    template<typename T>
    T guess(){
        static T upperBound;
        static T lowerBound;
        static T anserw;
        switch (feedbackFlag){
            case 2:
                upperBound = std::numeric_limits<T>::max();
                lowerBound = std::numeric_limits<T>::min();
                break;
            case 1:
                upperBound = anserw;
                break;
            case -1:
                lowerBound = anserw;
            break;
        }
        anserw = generateQuest<T>(lowerBound, upperBound);
        return anserw;
    }
};

#endif // PLAYER_H
