#ifndef QUESTS_H
#define QUESTS_H
#include <exception>
#include "util.h"
#include <memory>




template <class T>
class Quests
{
    int N;
    T* table;
public:

    class iterator: public std::iterator<std::forward_iterator_tag, T>{
        T* ptr;
    public:
        iterator(const Quests<T>::iterator &it){
            ptr = it.ptr;
        }
        iterator(T* pointer = nullptr): ptr(pointer){}
        iterator operator++(){
            iterator i =*this;
            ptr++;
            return i;
        }
        iterator operator++(int){
            ptr++;
            return *this;
        }
        T& operator*(){return *ptr;}
        T* operator->(){return ptr;}
        bool operator==(const iterator &it){ return ptr == it.ptr;}
        bool operator!=(const iterator &it){ return ptr != it.ptr;}
    };

    Quests():N{0}, table{nullptr}{}
    Quests(int n):N{n}, table{new T(N)}{
        if(n<0){
            std::bad_alloc except;
            throw except;
        }
        for(int i =0; i<N; i++){
            table[i] = (generateQuest<T>());
        }
    }
    ~Quests(){delete [] table;}

    void newQuestion(){
        T *buffor = new T(N);
        for(int i = 0; i<N; i++){
            buffor[i] = table[i];
        }
        N+=1;
        table = new T[N];
        for(int i = 0; i<(N-1); i++){
            table[i] = buffor[i];
        }
        table[N-1] = generateQuest<T>();
        delete [] buffor;
    }

    int size(){
        return N;
    }

    T& operator [](int index){
        if(index<0 || index >= N){
            throw std::out_of_range{"Out of range!\n"};
        }
        return table[index];
    }

    iterator begin(){
        iterator it{&table[0]};
        return it;
    }
    iterator end(){
        iterator it{&table[N]};
        return it;
    }
};



#endif // QUESTS_H
