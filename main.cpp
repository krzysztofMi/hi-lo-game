#include <iostream>

#include "Quests.h"
#include "Player.h"
#include <vector>
int main(){
    Quests<int> quests(3);
    quests.newQuestion();
    quests.newQuestion();
    for(int i = 0; i < quests.size(); i++)
       std::cout << quests[i] << std::endl;

    std::cout << "---------------------------------" << std::endl;

    Quests<int>::iterator iter = quests.begin();
    auto iter2(iter);
    for(;iter != quests.end(); iter++, ++iter2)
        std::cout << *iter << std::endl << *iter2 << std::endl;
}
