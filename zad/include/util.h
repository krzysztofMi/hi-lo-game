#ifndef UTIL_H
#define UTIL_H
#include <limits>
#include <random>
#include <chrono>

template <typename T>
T generateQuest(){
    auto seed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
    std::mt19937 gen(seed);
    std::uniform_int_distribution<T> dist(std::numeric_limits<T>::min(),
                                          std::numeric_limits<T>::max());
    return dist(gen);
}

template <>
float generateQuest<float>(){
    auto seed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
    std::mt19937 gen(seed);
    std::uniform_real_distribution<float> dist(std::numeric_limits<float>::min(),
                                          std::numeric_limits<float>::max());
    return dist(gen);
}

template <>
double generateQuest<double>(){
    auto seed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
    std::mt19937 gen(seed);
    std::uniform_real_distribution<double> dist(std::numeric_limits<double>::min(),
                                          std::numeric_limits<double>::max());
    return dist(gen);
}

#endif // UTIL_H
